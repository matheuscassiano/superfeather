
.p816   ; 65816 processor
.a16
.i16
.smart

.include "../../inc/engine/audio.inc"
.include "../../inc/engine/constants.inc"
.include "../../inc/engine/graphics.inc"

.import SetupPpuRegisters:far, BeginSprites, UpdateObjects, FinalizeSprites, sceneThinkFunc, screenBrightness
.import Music1

.export MySceneInit, MySceneThink
 
.segment "CODE0"

;                            OBJSEL,                                BGMODE,    BG1TILE,                                      BG2TILE,                                      BG3TILE,                                      BG4TILE,   BG12CHAR,                                      BG34CHAR,                                      MODE7SEL,  WIN12SEL,  WIN34SEL,  WINOBJSEL, WINBGLOG,  WINOBJLOG, DESTMAIN,  DESTSUB,   WINMAIN,   WINSUB,    CMATHSEL,  CMATHDEST, FIXEDCOLOR,        SETINI
Define_PpuDef SceneInitRegs, PPU_OBJSIZE_16_32 | PPU_OBJCHARADDR_0, %00001001, PPU_TILEMAPSIZE_64_32 | PPU_TILEMAPADDR_6000, PPU_TILEMAPSIZE_64_64 | PPU_TILEMAPADDR_6800, PPU_TILEMAPSIZE_32_64 | PPU_TILEMAPADDR_7800, %00000000, PPU_BG13CHARADDR_2000 | PPU_BG24CHARADDR_2000, PPU_BG13CHARADDR_2000 | PPU_BG24CHARADDR_2000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00011111, %00000000, %00000000, %00000000, %00000000, %00000000, %0000000000000000, %00000000


; ============================================================================
; The initialization function, called once during startup of this scene
; ============================================================================
MySceneInit:
    php
    
    rep #$10
    sep #$20
    
    ; Audio - stop sounds and music, go to uploader
    Macro_SpcCommand #SPC_CMD_MUSIC_STOP, #$00
    Macro_SpcCommand #SPC_CMD_STOP_ALL_SOUNDS, #$00
    Macro_SpcCommand #SPC_CMD_LOAD, #$00
    
    ; Initialize the registers using the data structure above.
    ldx #.loword(SceneInitRegs)
    jsl SetupPpuRegisters

    ; Load palettes, VRAM data, etc. here.
    
    ; Audio - upload and play music
    Macro_SpcUploadMusic Music1
    Macro_SpcCommand #SPC_CMD_MUSIC_PLAY, #$0000
    
    rep #$20
    
    lda #.loword(MySceneThink)
    sta sceneThinkFunc
    
    plp
    rts


; ============================================================================
; The thinker function for this scene, called every frame.
; ============================================================================
MySceneThink:
    php
    
    sep #$20
    lda #%00001111
    sta screenBrightness
    rep #$30
    
    jsr BeginSprites
    jsr UpdateObjects
    jsr FinalizeSprites
    
    plp
    rts
