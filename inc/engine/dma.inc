; Special thanks to bazz, Neviksti, Marc, the original authors of these macros.
; They have been ported to ca65.

.p816   ; 65816 processor
.smart

.import LoadPalette, LoadVRAM, LoadWRAM

;============================================================================
; LoadPalette - Macro that loads palette information into CGRAM
;----------------------------------------------------------------------------
; Modifies: A,X
; Requires: mem/A = 8 bit, X/Y = 16 bit
;----------------------------------------------------------------------------
.macro Macro_LoadPalette SOURCE, START, DATASIZE
    lda START
    sta $2121        ; Start at START color
    lda #<.bank(SOURCE)
    ldx #.loword(SOURCE)
    ldy DATASIZE
    jsr LoadPalette
.endmacro

;============================================================================
; LoadVRAM -- Macro that simplifies calling LoadVRAM to copy data to VRAM
;----------------------------------------------------------------------------
; Modifies: A, X, Y
;----------------------------------------------------------------------------
.macro Macro_LoadVRAM SOURCE, DESTINATION, DATASIZE
    lda #$80
    sta $2115       ; Set VRAM transfer mode to word-access, increment by 1

    ldx DESTINATION
    stx $2116       ; $2116: Word address for accessing VRAM.
    lda #<.bank(SOURCE)
    ldx #.loword(SOURCE)
    ldy DATASIZE
    jsr LoadVRAM
.endmacro

;============================================================================
; LoadWRAM -- Macro that simplifies calling LoadWRAM to copy data to WRAM
;----------------------------------------------------------------------------
; Modifies: A, X, Y
;----------------------------------------------------------------------------

.macro Macro_LoadWRAM SOURCE, DESTINATION, DATASIZE
    ldx #.loword(DESTINATION)
    stx $2181       ; $2181: Word address for accessing WRAM.
    lda #<.bank(DESTINATION)
    sta $2183
    lda #<.bank(SOURCE)
    ldx #.loword(SOURCE)
    ldy DATASIZE
    jsr LoadWRAM
.endmacro
