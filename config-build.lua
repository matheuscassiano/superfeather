
return
{
    fileList =
    {
        -- Engine files
        "src/engine/init.asm",
        "src/engine/utils.asm",
        "src/engine/initsnes.asm",
        "src/engine/crashhandler.asm",
        "src/engine/gameloop.asm",
        "src/engine/audio.asm",
        "src/engine/dma.asm",
        "src/engine/graphics.asm",
        "src/engine/dmamanager.asm",
        "src/engine/gameobjects.asm",
        
        -- Game files
        "src/game/header.asm",
        "src/game/main.asm",
    },
    target = "superfeather",
    linkerConfig = "config-ca65.cfg"
}
