
return
{
    fileList =
    {
        -- Engine files
        "src/engine/init.asm",
        "src/engine/utils.asm",
        "src/engine/initsnes.asm",
        "src/engine/crashhandler.asm",
        "src/engine/gameloop.asm",
        "src/engine/audio.asm",
        "src/engine/dma.asm",
        "src/engine/graphics.asm",
        "src/engine/dmamanager.asm",
        "src/engine/gameobjects.asm",
        
        -- Game files
        "examples/helloworld/header.asm",
        "examples/helloworld/main.asm",
        "examples/helloworld/helloworld.asm",
    },
    target = "superfeather-ex-helloworld",
    linkerConfig = "examples/config-ca65.cfg"
}
