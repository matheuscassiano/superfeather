
.p816   ; 65816 processor
.smart

.include "../../inc/engine/constants.inc"
.include "../../inc/engine/graphics.inc"

.segment "CODE0"

;                     Name,             X pos, Y pos, sprite size,  tile, vhoopppN, next sprite
Define_SpriteDef      MySprite_1,       $0000, $0000, SPRITE_SMALL, $40, %00100000, 0
Define_SpriteDef      MySprite_2,       $0000, $0000, SPRITE_SMALL, $42, %00100000, 0

Define_SpriteDef      MyMetasprite_1,   $0000, $0000, SPRITE_LARGE, $00, %00100000, MyMetasprite_1a
Define_SpriteDef      MyMetasprite_1a,  $0020, $0000, SPRITE_LARGE, $04, %00100000, 0

;                     Name,             delay, next anim,      sprite/callback
Define_AnimDef        MyAnimDef_1,      5,     MyAnimDef_2,    MySprite_1
Define_AnimDef        MyAnimDef_2,      5,     MyAnimDef_1,    MySprite_2
