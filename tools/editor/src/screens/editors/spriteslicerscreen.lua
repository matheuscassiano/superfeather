local app = require "src.app"
local extramath = require "src.utils.extramath"
local test = require "src.utils.test"

local EditorScreen = require "src.screens.editors.editorscreen"
local EditorAction = require "src.screens.editors.editoraction"
local SerializedTable = require "src.dataobjs.serializedtable"

local CharData = require "src.dataobjs.chardata"
local PaletteData = require "src.dataobjs.palettedata"
local SpriteManagerScreen = require "src.screens.editors.spritemanagerscreen"

local maxZoom = 16

local helpString = "S - Save"
                   .. "\n\nTab - Manage sprites"
                   .. "\n\nPgUp/PgDn - Switch HW sprite"
                   .. "\nCtrl + PgUp/PgDn - Switch metasprite"
                   .. "\nIns/Del - Add/delete HW sprite"
                   .. "\nCtrl + Ins/Del - Add/delete metasprite"
                   .. "\n\n[Ctrl] + 0-7 - Change palette"
                   .. "\nShift + Arrow keys - Nudge HW sprite"
                   .. "\nL - Toggle HW sprite size"

local spriteSizePairs =
{
    {
        small = 8,
        large = 16
    },
    {
        small = 8,
        large = 32
    },
    {
        small = 16,
        large = 32
    }
}

local AddToTableAction = EditorAction:makeClass
{
    insertIndex = 0
}

function AddToTableAction:init(spriteSlicerScreen, item, destTable, index)
    self.editor = spriteSlicerScreen
    self.destTable = destTable
    self.insertIndex = index or #destTable + 1
    self.item = item
end

function AddToTableAction:undo()
    table.remove(self.destTable, self.insertIndex)
end

function AddToTableAction:redo()
    table.insert(self.destTable, self.insertIndex, self.item)
end

function AddToTableAction:isChange()
    return true
end


local DeleteFromTableAction = EditorAction:makeClass
{
    index = 0
}

function DeleteFromTableAction:init(spriteSlicerScreen, index, destTable)
    self.editor = spriteSlicerScreen
    
    if not destTable[index] then
        self.change = false
    else
        self.change = true
        self.destTable = destTable
        self.index = index
    end
end

function DeleteFromTableAction:undo()
    if self.change then
        table.insert(self.destTable, self.index, self.item)
    end
end

function DeleteFromTableAction:redo()
    if self.change then
        self.item = table.remove(self.destTable, self.index)
    end
end

function DeleteFromTableAction:isChange()
    return self.change
end


local ModifyTableAction = EditorAction:makeClass
{
    
}

function ModifyTableAction:init(spriteSlicerScreen, destTable, key, value)
    self.editor = spriteSlicerScreen
    
    if not destTable then
        self.change = false
    else
        self.destTable = destTable
        self.key = key
        self.newValue = value
        self.oldValue = destTable[key]
        self.change = (self.oldValue ~= self.newValue)
    end
end

function ModifyTableAction:undo()
    if self.change then
        self.destTable[self.key] = self.oldValue
    end
end

function ModifyTableAction:redo()
    if self.change then
        self.destTable[self.key] = self.newValue
    end
end

function ModifyTableAction:isChange()
    return self.change
end


local ReorderTableAction = EditorAction:makeClass
{
    
}

function ReorderTableAction:init(spriteSlicerScreen, destTable, oldIndex, newIndex)
    self.editor = spriteSlicerScreen
    
    if not destTable then
        self.change = false
    else
        self.destTable = destTable
        self.oldIndex = oldIndex
        self.newIndex = newIndex
        self.change = (oldIndex ~= newIndex and oldIndex > 0 and newIndex > 0
                       and oldIndex <= #destTable and newIndex <= #destTable)
    end
end

function ReorderTableAction:undo()
    if self.change then
        local item = table.remove(self.destTable, self.newIndex)
        table.insert(self.destTable, self.oldIndex, item)
    end
end

function ReorderTableAction:redo()
    if self.change then
        local item = table.remove(self.destTable, self.oldIndex)
        table.insert(self.destTable, self.newIndex, item)
    end
end

function ReorderTableAction:isChange()
    return self.change
end


local SpriteSlicerScreen = EditorScreen:makeClass
{
    handlers =
    {
        keypressed = EditorScreen.handlers.keypressed,
        filedropped = EditorScreen.handlers.filedropped,
        mousepressed = EditorScreen.handlers.mousepressed,
        mousereleased = EditorScreen.handlers.mousereleased,
        mousemoved = EditorScreen.handlers.mousemoved,
        wheelmoved = EditorScreen.handlers.wheelmoved,
    },
    clipboardId = "SpriteSlicer",
    currentMetasprite = 1,
    currentSprite = 1,
    zoomLevel = 4,
    columns = 16
}

SpriteSlicerScreen.filetypeMapping =
{
    {
        extension = ".pic",
        description = "SNES char data",
        handler = function(self, file)
            self:setMetaspriteChar(file)
        end
    },
    {
        extension = ".pal",
        description = "SNES palette",
        handler = function(self, file)
            self:setMetaspritePalette(file)
        end
    }
}

SpriteSlicerScreen.keyHandlers =
{
    s = function(self, isRepeat)
        if not isRepeat then
            self:saveToFile()
            self.unsaved = false
        end
    end,
    left = function(self, isRepeat)
        if love.keyboard.isDown("lshift", "rshift") then
            self:nudgeCurrentSprite(-1, 0)
        end
    end,
    right = function(self, isRepeat)
        if love.keyboard.isDown("lshift", "rshift") then
            self:nudgeCurrentSprite(1, 0)
        end
    end,
    up = function(self, isRepeat)
        if love.keyboard.isDown("lshift", "rshift") then
            self:nudgeCurrentSprite(0, -1)
        end
    end,
    down = function(self, isRepeat)
        if love.keyboard.isDown("lshift", "rshift") then
            self:nudgeCurrentSprite(0, 1)
        end
    end,
    l = function(self, isRepeat)
        self:toggleCurrentSpriteSize()
    end,
    o = function(self, isRepeat)
        self:toggleSpriteSizePair()
    end,
    insert = function(self, isRepeat)
        if love.keyboard.isDown("lctrl", "rctrl") then
            self:insertMetasprite()
        else
            self:insertSprite()
        end
    end,
    delete = function(self, isRepeat)
        if love.keyboard.isDown("lctrl", "rctrl") then
            self:deleteMetasprite()
        else
            self:deleteSprite()
        end
    end,
    pageup = function(self, isRepeat)
        if love.keyboard.isDown("lctrl", "rctrl") then
            self:selectPreviousMetasprite()
        else
            self:selectPreviousSprite()
        end
    end,
    pagedown = function(self, isRepeat)
        if love.keyboard.isDown("lctrl", "rctrl") then
            self:selectNextMetasprite()
        else
            self:selectNextSprite()
        end
    end,
    tab = function(self)
        app.pushScreen(SpriteManagerScreen:new(self))
    end,
    ["`"] = function(self, isRepeat)
        self:paletteKeyPressed(0)
    end,
    ["0"] = function(self, isRepeat)
        self:paletteKeyPressed(0)
    end,
    ["1"] = function(self, isRepeat)
        self:paletteKeyPressed(1)
    end,
    ["2"] = function(self, isRepeat)
        self:paletteKeyPressed(2)
    end,
    ["3"] = function(self, isRepeat)
        self:paletteKeyPressed(3)
    end,
    ["4"] = function(self, isRepeat)
        self:paletteKeyPressed(4)
    end,
    ["5"] = function(self, isRepeat)
        self:paletteKeyPressed(5)
    end,
    ["6"] = function(self, isRepeat)
        self:paletteKeyPressed(6)
    end,
    ["7"] = function(self, isRepeat)
        self:paletteKeyPressed(7)
    end,
    ["-"] = function(self, isRepeat)
        if self.zoomLevel > 1 then
            self.zoomLevel = self.zoomLevel - 1
            self.dirty = true
        end
    end,
    ["="] = function(self, isRepeat)
        if self.zoomLevel < maxZoom then
            self.zoomLevel = self.zoomLevel + 1
            self.dirty = true
        end
    end,
}

function SpriteSlicerScreen:init(filepath)
    EditorScreen.init(self)

    self.filepath = filepath
    self.projectpath = self.filepath .. "/project.luatexts"
    
    self.paletteData = PaletteData:new()
    self.paletteData:loadStandard16()
    self.paletteData:padTo256()
    
    self:loadProjectFile()
    
    self.title = filepath
    self:setTitle()
end

function SpriteSlicerScreen:undo()
    EditorScreen.undo(self)
    
    self:clampCurrentIndex()
    self:updateCurrentChar()
end

function SpriteSlicerScreen:redo()
    EditorScreen.redo(self)
    
    self:clampCurrentIndex()
    self:updateCurrentChar()
end

function SpriteSlicerScreen:insertMetasprite()
    local metasprite =
    {
        name = self:getNewMetaspriteName("NewMetasprite"),
        sprites = {}
    }
    self:doEditAction(AddToTableAction:new(self, metasprite,
            self.project.data.metasprites, self.currentMetasprite + 1))
    
    self:undoCheckpoint()
    self.currentMetasprite = self.currentMetasprite + 1
    self:clampCurrentIndex()
    
    self:undoCheckpoint()
end

function SpriteSlicerScreen:deleteMetasprite()
    if #self.project.data.metasprites > 0 then
        self:undoCheckpoint()
        self:doEditAction(DeleteFromTableAction:new(self, self.currentMetasprite, self.project.data.metasprites))
        self:undoCheckpoint()
    end
end

function SpriteSlicerScreen:insertSprite()
    local metasprite = self.project.data.metasprites[self.currentMetasprite]
    
    if not metasprite then return end
    
    if not metasprite.sprites then
        metasprite.sprites = {}
    end
    
    local sprite =
    {
        x = 0,
        y = 0,
        large = false
    }
    
    self:undoCheckpoint()
    self:doEditAction(AddToTableAction:new(self, sprite, metasprite.sprites))
    self.currentSprite = #metasprite.sprites
    
    self:undoCheckpoint()
end

function SpriteSlicerScreen:deleteSprite()
    local metasprite = self.project.data.metasprites[self.currentMetasprite]
    
    if not metasprite then return end
    
    if metasprite.sprites and #metasprite.sprites > 0 then
        self:undoCheckpoint()
        self:doEditAction(DeleteFromTableAction:new(self, self.currentSprite, metasprite.sprites))
        self:undoCheckpoint()
    end
end

function SpriteSlicerScreen:selectNextMetasprite()
    self.currentMetasprite = self.currentMetasprite + 1
    
    self:updateCurrentChar()
    self:clampCurrentIndex()
    self.dirty = true
    self:undoCheckpoint()
end

function SpriteSlicerScreen:selectPreviousMetasprite()
    self.currentMetasprite = self.currentMetasprite - 1
    
    self:updateCurrentChar()
    self:clampCurrentIndex()
    self.dirty = true
    self:undoCheckpoint()
end

function SpriteSlicerScreen:selectNextSprite()
    self.currentSprite = self.currentSprite + 1
    
    self:clampCurrentIndex()
    self.dirty = true
    self:undoCheckpoint()
end

function SpriteSlicerScreen:selectPreviousSprite()
    self.currentSprite = self.currentSprite - 1
    
    self:clampCurrentIndex()
    self.dirty = true
    self:undoCheckpoint()
end

function SpriteSlicerScreen:reorderMetaspriteForward()
    self:doEditAction(ReorderTableAction:new(self, self.project.data.metasprites, self.currentMetasprite, self.currentMetasprite + 1))
    
    self.currentMetasprite = self.currentMetasprite + 1
    
    self:clampCurrentIndex()
    self.dirty = true
    self:undoCheckpoint()
end

function SpriteSlicerScreen:reorderMetaspriteBack()
    self:doEditAction(ReorderTableAction:new(self, self.project.data.metasprites, self.currentMetasprite, self.currentMetasprite - 1))
    
    self.currentMetasprite = self.currentMetasprite - 1
    
    self:clampCurrentIndex()
    self.dirty = true
    self:undoCheckpoint()
end

function SpriteSlicerScreen:reorderSpriteForward()
    local metasprite = self.project.data.metasprites[self.currentMetasprite]
    
    if not metasprite then return end
    
    self:doEditAction(ReorderTableAction:new(self, metasprite.sprites, self.currentSprite, self.currentSprite + 1))
    
    self.currentSprite = self.currentSprite + 1
    
    self:clampCurrentIndex()
    self.dirty = true
    self:undoCheckpoint()
end

function SpriteSlicerScreen:reorderSpriteBack()
    local metasprite = self.project.data.metasprites[self.currentMetasprite]
    
    if not metasprite then return end
    
    self:doEditAction(ReorderTableAction:new(self, metasprite.sprites, self.currentSprite, self.currentSprite - 1))
    
    self.currentSprite = self.currentSprite - 1
    
    self:clampCurrentIndex()
    self.dirty = true
    self:undoCheckpoint()
end

function SpriteSlicerScreen:renameMetasprite(newName)
    local metasprite = self.project.data.metasprites[self.currentMetasprite]
    
    if not metasprite then return end
    
    self:doEditAction(ModifyTableAction:new(self, metasprite, "name", newName))
    
    self.dirty = true
    self:undoCheckpoint()
end

function SpriteSlicerScreen:clampCurrentIndex()
    self.currentMetasprite = extramath.clampValue(1, #self.project.data.metasprites, self.currentMetasprite)
    
    local metasprite = self.project.data.metasprites[self.currentMetasprite]
    
    if metasprite and metasprite.sprites then
        self.currentSprite = extramath.clampValue(1, #metasprite.sprites, self.currentSprite)
    else
        self.currentSprite = 1
    end
end

function SpriteSlicerScreen:saveToFile()
    self.project.filepath = self.projectpath
    self.project:saveToFile()
end

function SpriteSlicerScreen:loadProjectFile()
    if SerializedTable.exists(self.projectpath) then
        self.project = SerializedTable:new(self.projectpath)
    else
        self:newProjectFile()
    end
    
    if not self.project.data.metasprites then
        self.project.data.metasprites = {}
    end
    
    if #self.project.data.metasprites == 0 then
        self.project.data.metasprites[1] = {}
    end
    
    if not self.project.data.spriteSizePair then
        self.project.data.spriteSizePair = 1
    end
    
    for i, v in ipairs(self.project.data.metasprites) do
        if v.name == nil or v.name == "" then
            v.name = self:getNewMetaspriteName("UnknownMetasprite")
        end
    end
    
    self:updateCurrentChar()
end

function SpriteSlicerScreen:newProjectFile()
    
    self.project = SerializedTable:new()
    self.project.data =
    {
        outputChar = "out.char",
        outputAsm = "out.asm",
        spriteSizePair = 1,
        metasprites = {}
    }
end

function SpriteSlicerScreen:checkMetaspriteName(newName)
    for i, v in ipairs(self.project.data.metasprites) do
        if v.name == newName then
            return false
        end
    end
    
    return true
end

function SpriteSlicerScreen:getNewMetaspriteName(newName)
    local duplicateCount = 0
    local nameCandidate = newName
    
    while true do
        if self:checkMetaspriteName(nameCandidate) then
            break
        end
        
        duplicateCount = duplicateCount + 1
        
        nameCandidate = newName .. "_" .. tostring(duplicateCount)
    end
    
    return nameCandidate
end

function SpriteSlicerScreen:paletteKeyPressed(index)
    index = index * 16
    
    if love.keyboard.isDown("lctrl", "rctrl") then
        index = index + 128
    end
    
    self:setMetaspritePaletteOffset(index)
end

function SpriteSlicerScreen:updateCurrentChar()
    local metasprite = self.project.data.metasprites[self.currentMetasprite]
    
    if not metasprite then return end
    
    self.paletteOffset = metasprite.paletteOffset
    
    self:newChar(metasprite.charPath, true)
    self:newPalette(metasprite.palettePath, true)
end

function SpriteSlicerScreen:setMetaspriteChar(filepath)
    local metasprite = self.project.data.metasprites[self.currentMetasprite]
    
    if not metasprite then return end
    
    self:undoCheckpoint()
    self:doEditAction(ModifyTableAction:new(self, metasprite, "charPath", filepath))
    self:undoCheckpoint()
    
    self:newChar(filepath, false, true)
end

function SpriteSlicerScreen:setMetaspritePalette(filepath)
    local metasprite = self.project.data.metasprites[self.currentMetasprite]
    
    if not metasprite then return end
    
    self:undoCheckpoint()
    self:doEditAction(ModifyTableAction:new(self, metasprite, "palettePath", filepath))
    self:undoCheckpoint()
    
    self:newPalette(filepath, true)
end

function SpriteSlicerScreen:setMetaspritePaletteOffset(offset)
    local metasprite = self.project.data.metasprites[self.currentMetasprite]
    
    if not metasprite then return end
    
    self:undoCheckpoint()
    self:doEditAction(ModifyTableAction:new(self, metasprite, "paletteOffset", offset))
    self:undoCheckpoint()
    
    self.paletteOffset = metasprite.paletteOffset
    
    self:newTexture()
end

function SpriteSlicerScreen:newChar(filepath, dontRefreshTexture)
    if not filepath then
        self.charData = nil
    else
        self.charData = CharData:new(filepath)
    end
    
    if not dontRefreshTexture then
        self:newTexture()
    end
    
    self.charDataPath = filepath
end

function SpriteSlicerScreen:newPalette(filepath)
    if not filepath then
        self.paletteData = PaletteData:new()
        self.paletteData:loadStandard16()
    else
        self.paletteData = PaletteData:new(filepath)
    end
    
    self.paletteData:padTo256()
    self:newTexture()
    
    self.paletteDataPath = filepath
end

function SpriteSlicerScreen:newTexture()
    if not self.charData then
        self.charTexture = nil
    else
        self.charTexture = self.charData:generateTexture(self.paletteData, self.columns, self.paletteOffset)
    end
    
    self.dirty = true
end

function SpriteSlicerScreen:nudgeCurrentSprite(offsetX, offsetY)
    local metasprite = self.project.data.metasprites[self.currentMetasprite]
    
    if not metasprite or not metasprite.sprites then return end
    
    local sprite = metasprite.sprites[self.currentSprite]
    
    if not sprite then return end
    
    local newX = sprite.x + offsetX
    local newY = sprite.y + offsetY
    
    self:doEditAction(ModifyTableAction:new(self, sprite, "x", newX))
    self:doEditAction(ModifyTableAction:new(self, sprite, "y", newY))
    
    self.dirty = true
end

function SpriteSlicerScreen:toggleSpriteSizePair()
    local newSpriteSizePair = self.project.data.spriteSizePair % #spriteSizePairs + 1
    
    self:undoCheckpoint()
    self:doEditAction(ModifyTableAction:new(self, self.project.data, "spriteSizePair", newSpriteSizePair))
    self:undoCheckpoint()
    
    self.dirty = true
end

function SpriteSlicerScreen:toggleCurrentSpriteSize()
    local metasprite = self.project.data.metasprites[self.currentMetasprite]
    
    if not metasprite or not metasprite.sprites then return end
    
    local sprite = metasprite.sprites[self.currentSprite]
    
    if not sprite then return end
    
    self:undoCheckpoint()
    self:doEditAction(ModifyTableAction:new(self, sprite, "large", not sprite.large))
    self:undoCheckpoint()
    
    self.dirty = true
end

function SpriteSlicerScreen:getWidthForSpriteSize(isLarge)
    local sizePairIndex = (self.project.data.spriteSizePair - 1) % #spriteSizePairs + 1
    local sizePair = spriteSizePairs[sizePairIndex]
    
    return isLarge and sizePair.large or sizePair.small
end

function SpriteSlicerScreen:drawContent(width, height)
    love.graphics.setFont(app.res.uifont)
    love.graphics.setColor(255, 255, 255)
    
    local metasprite = self.project.data.metasprites[self.currentMetasprite]
    
    if not metasprite then
        love.graphics.print("No metasprite. Press Ctrl + Ins to make a new one!")
        return
    elseif not metasprite.sprites then
        metasprite.sprites = {}
    end
    
    love.graphics.setLineWidth(2 / self.zoomLevel)
    love.graphics.setLineStyle("smooth")
    
    love.graphics.push()
    love.graphics.scale(self.zoomLevel, self.zoomLevel)
    
    if self.charTexture then
        love.graphics.draw(self.charTexture, 0, 0, 0, 1, 1)
    end
    
    for i, sprite in ipairs(metasprite.sprites) do
        if i ~= self.currentSprite then
            local size = self:getWidthForSpriteSize(sprite.large)
            love.graphics.rectangle("line", sprite.x, sprite.y, size, size)
        end
    end
    
    local currentSprite = metasprite.sprites[self.currentSprite]
    
    if currentSprite then
        love.graphics.setColor(255, 0, 0)
        local size = self:getWidthForSpriteSize(currentSprite.large)
        love.graphics.rectangle("line", currentSprite.x, currentSprite.y, size, size)
    end
    
    love.graphics.pop()
end

function SpriteSlicerScreen:drawSidebarMetaspriteInfo(width, height, metasprites)
    if not metasprites then return end
    
    local sizePairIndex = (self.project.data.spriteSizePair - 1) % #spriteSizePairs + 1
    local sizePair = spriteSizePairs[sizePairIndex]
    local spriteSizeString = sizePair.small .. "x" .. sizePair.small .. ", "
            .. sizePair.large .. "x" .. sizePair.large
    
    love.graphics.print("O - Change project sprite sizes", 16, 16)
    
    love.graphics.print(spriteSizeString, 128, 36)
    
    local metasprite = self.project.data.metasprites[self.currentMetasprite]
    local numSprites = metasprite and metasprite.sprites and #metasprite.sprites or 0
    local metaspriteString = "Metasprite"
    local metaspriteString2 = self.currentMetasprite .. " of " .. #self.project.data.metasprites
    
    if metasprite then
        metaspriteString = metaspriteString .. "\n" .. tostring(metasprite.name)
    end

    love.graphics.print(metaspriteString, 16, 80)
    love.graphics.print(metaspriteString2, 128, 80)
    
    if not metasprite then return end
    
    if metasprite.sprites then
        local sprite = metasprite.sprites[self.currentSprite]
        
        if sprite then
            local spriteString = "HW Sprite" .. "\n\tX offset" .. "\n\tY offset" .. "\n\tSize "
            local spriteString2 = self.currentSprite .. " of " .. numSprites
                    .. "\n" .. sprite.x .. "\n" .. sprite.y .. "\n" .. (sprite.large and "Large" or "Small")
            
            love.graphics.print(spriteString, 16, 136)
            love.graphics.print(spriteString2, 128, 136)
        end
    end
    
    local charPathString = metasprite.charPath or "Please provide a .pic"
    local palettePathString = metasprite.palettePath or "Please provide a .pal"
    
    love.graphics.setFont(app.res.uifontSmall)
    
    love.graphics.printf(charPathString .. "\n" .. palettePathString, 32, 240, width - 48, "right")
end

function SpriteSlicerScreen:drawSidebar(width, height)
    test.unusedArgs(width)
    love.graphics.setFont(app.res.uifont)
    love.graphics.setColor(255, 255, 255)
    
    self:drawSidebarMetaspriteInfo(width, height, self.project.data.metasprites)
    
    local zoomTextHeight = height - 32
    
    love.graphics.setFont(app.res.uifontSmall)
    if height >= 544 then
        love.graphics.print(helpString, 16, height - (11*16) - 24)
        
        zoomTextHeight = zoomTextHeight - (12*16)
    end
    
    love.graphics.print("-/= - Zoom", 16, zoomTextHeight)
    love.graphics.print(math.floor(self.zoomLevel * 100) .. "%", 128, zoomTextHeight)
end

return SpriteSlicerScreen
