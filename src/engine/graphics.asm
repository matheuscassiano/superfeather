
.p816   ; 65816 processor
.a16
.i16
.smart

.include "../../inc/engine/constants.inc"

.export ClearSprites, AddMetasprite, AddMetaspriteBoundingBox, AddSimpleSprite, BeginSprites, FinalizeSprites
.export SetupPpuRegisters:far
.export spriteScrollX, spriteScrollY, bg1ScrollX, bg1ScrollY, bg2ScrollX, bg2ScrollY, bg3ScrollX, bg3ScrollY, spriteTable
.import objPosX, objPosY, objAttribute

.segment "ZEROPAGE"

_spriteTableScratch: .res $80
_lastOamPos: .res 2

.segment "LORAM"

spriteScrollX: .res 2
spriteScrollY: .res 2
bg1ScrollX: .res 2
bg1ScrollY: .res 2
bg2ScrollX: .res 2
bg2ScrollY: .res 2
bg3ScrollX: .res 2
bg3ScrollY: .res 2
_oamPos: .res 2

spriteTable: .res $0200
spriteTableHigh: .res $20

.segment "DATA1"


; ============================================================================
; SetupPpuRegisters - Initializes the PPU registers, using a PpuDef to set
;                     various modes.
; ---------------------------------------------------------------------------
; In: X -- PpuDef
; ---------------------------------------------------------------------------
; Expects anything, clobbers A, X, Y
; ============================================================================

SetupPpuRegisters:
    php
    phd
    rep #$30
    
    lda #$2100
    tcd
    
    sep #$20
    
    lda a:PPUDEF_OBJSEL, x
    sta $01
    lda a:PPUDEF_BGMODE, x
    sta $05
    lda #$00
    sta $06 ; mosaic
    lda a:PPUDEF_BG1TILEMAP, x
    sta $07
    lda a:PPUDEF_BG2TILEMAP, x
    sta $08
    lda a:PPUDEF_BG3TILEMAP, x
    sta $09
    lda a:PPUDEF_BG4TILEMAP, x
    sta $0a
    lda a:PPUDEF_BG12CHAR, x
    sta $0b
    lda a:PPUDEF_BG34CHAR, x
    sta $0c
    
    txy
    ldx #$0d
    
_SetBGScrollLoop:
    lda #$00
    sta z:$00, x
    sta z:$00, x
    lda #$ff
    sta z:$01, x
    sta z:$01, x
    inx
    inx
    cpx #$15
    bcc _SetBGScrollLoop

    lda #$00
    ldx #$1b
    
_SetMode7ScrollLoop:
    sta z:$00, x
    sta z:$00, x
    inx
    cpx #$21
    bcc _SetMode7ScrollLoop
    
    tyx
    
    lda a:PPUDEF_MODE7SEL, x
    sta $1a
    
    lda a:PPUDEF_WIN12SEL, x
    sta $23
    lda a:PPUDEF_WIN34SEL, x
    sta $24
    lda a:PPUDEF_WINOBJSEL, x
    sta $25
    
    stz $26 ; Window pos registers
    stz $27
    stz $28
    stz $29
    
    lda a:PPUDEF_WINBGLOGIC, x
    sta $2a
    lda a:PPUDEF_WINOBJLOGIC, x
    sta $2b
    lda a:PPUDEF_DESTMAIN, x
    sta $2c
    lda a:PPUDEF_DESTSUB, x
    sta $2d
    lda a:PPUDEF_WINMAIN, x
    sta $2e
    lda a:PPUDEF_WINSUB, x
    sta $2f
    lda a:PPUDEF_COLORMATHSEL, x
    sta $30
    lda a:PPUDEF_COLORMATHDEST, x
    sta $31
    
    ; Special logic for fixed color
    ; Blue
    lda a:PPUDEF_FIXEDCOLOR + 1, x
    lsr
    lsr
    ora #%10000000
    sta $32
    ; Green
    rep #$20
    lda a:PPUDEF_FIXEDCOLOR, x
    lsr
    lsr
    lsr
    lsr
    lsr
    and #%00011111
    sep #$20
    ora #%01000000
    sta $32
    ; Red
    lda a:PPUDEF_FIXEDCOLOR, x
    and #%00011111
    ora #%00100000
    sta $32
    
    lda a:PPUDEF_SETINI, x
    sta $33
    
    pld
    plp
    rtl

.segment "CODE0"


; ============================================================================
; CkearSprites - Removes all sprites. Intended for initialization routines.
; ---------------------------------------------------------------------------
; Expects anything, clobbers A, X, Y
; ============================================================================
ClearSprites:
    php
    sep #$20
    rep #$10
    
    stz _lastOamPos
    
    ; Set X to be off-screen
    ldx #$0000
    ldy #$0000
    
_ClearOffscreenLoop:
    lda #$01
    sta spriteTable + OAM_X, x
    sta _spriteTableScratch, y
    lda #$55
    sta spriteTableHigh, y
    inx
    inx
    inx
    inx
    iny
    cpx #$0200
    bcc _ClearOffscreenLoop
    
    plp
    rts


; ============================================================================
; AddMetasprite - Add metasprite's sprite entries to OAM
; ---------------------------------------------------------------------------
; In: X -- Object index
;     Y -- Spritedef
; ---------------------------------------------------------------------------
; Expects AXY16, clobbers A, D
; ============================================================================
AddMetasprite:
    rep #$71 ; Also clears overflow, carry bits
    
    lda _oamPos
    cmp #$0080
    bcs _NoMoreSprites ; Hit sprite limit, bail out

_AddMetaspritePassedBoundsCheck:
    
    ; D is now address of OAM table destination
    asl
    asl
    adc #spriteTable
    tcd
    
    ; Check Y is in range
    lda objPosY, x
    
    ; Y scroll and offset
    adc a:SPRITEDEF_Y, y
    sec
    sbc spriteScrollY
    
    cmp #$ffe0  ; Check if in negative Y region of screen
    bcs _SpriteYInRange
    cmp #$00e0  ; If greater than 224, it's offscreen
    bcs _SpriteOutside
    
_SpriteYInRange:
    sta z:OAM_Y
    
    ; Check X is in range or is negative (65536 - 32 = 65504 = $ffe0)
    lda objPosX, x
    
    ; X scroll and offset
    clc
    adc a:SPRITEDEF_X, y
    sec
    sbc spriteScrollX
    
    cmp #$ffe0  ; Check if in negative X region of screen
    bcs _SpriteXInRange
    cmp #$0100  ; If greater than 256, it's offscreen
    bcs _SpriteOutside
    
_SpriteXInRange:
    
    sep #$20
    
    ; X position
    sta z:OAM_X
    
    ; High OAM scratch
    xba
    and #$01
    ora a:SPRITEDEF_SIZE, y
    
    phx
    
    ldx _oamPos
    sta a:_spriteTableScratch, x
    
    plx
    
    ; Attributes
    rep #$21
    lda objAttribute, x
    
    adc a:SPRITEDEF_ATTRIBUTES, y
    sta z:OAM_ATTRIBUTES
    
    ; Increment position in OAM tables
    inc _oamPos
    
    ; Check for next spritedef
    lda a:SPRITEDEF_NEXTDEF, y
    bne _NextSprite
    
    rts
    
_SpriteOutside:
    
    ; Check for next spritedef
    lda a:SPRITEDEF_NEXTDEF, y
    bne _NextSprite
    
_NoMoreSprites:
    
    sep #$40 ; Overflow flag indicates sprite was not placed
    rts

_NextSprite:
    
    tay
    bra AddMetasprite

; ============================================================================
; AddMetaspriteBoundingBox - Add metasprite's sprite entries to OAM if
;                            bounding box is within the screen.
; ---------------------------------------------------------------------------
; In: X -- Object index
;     Y -- BoundingBoxDef
; ---------------------------------------------------------------------------
; Expects AXY16, clobbers A, Y
; ============================================================================
AddMetaspriteBoundingBox:
    rep #$71 ; Also clears overflow, carry bits
    
    lda _oamPos
    cmp #$0080
    bcs _NoMoreSprites ; Hit sprite limit, bail out
    
    ; Check X is in range
    lda objPosX, x
    
    ; X scroll and offset
    clc
    adc a:BOUNDINGBOXDEF_LEFT, y
    sec
    sbc spriteScrollX
    bmi _BoundingBoxOffLeft ; If negative, assume less than 256
    
    cmp #$0100 ; If greater than 256, it's offscreen
    bcs _NoMoreSprites
    
_BoundingBoxOffLeft:
    adc a:BOUNDINGBOXDEF_WIDTH, y
    bmi _NoMoreSprites ; If less than 0, it's offscreen
    
    ; Check Y is in range
    lda objPosY, x

    ; Y scroll and offset
    clc
    adc a:BOUNDINGBOXDEF_TOP, y
    sec
    sbc spriteScrollY
    bmi _BoundingBoxOffTop ; If negative, assume less than 224
    
    cmp #$00e0 ; If greater than 224, it's offscreen
    bcs _NoMoreSprites
    
_BoundingBoxOffTop:
    adc a:BOUNDINGBOXDEF_HEIGHT, y
    bmi _NoMoreSprites ; If less than 0, it's offscreen
    
    lda a:BOUNDINGBOXDEF_DATA, y
    tay
    
    lda _oamPos
    jmp _AddMetaspritePassedBoundsCheck

; ============================================================================
; AddSimpleSprite - Add single sprite entry to OAM. objAttribute is ignored.
; ---------------------------------------------------------------------------
; In: X -- Object index
;     Y -- Spritedef
; ---------------------------------------------------------------------------
; Expects AXY16, clobbers A, D
; ============================================================================
AddSimpleSprite:
    rep #$71 ; Also clears overflow, carry bits
    
    lda _oamPos
    cmp #$0080
    bcs _NoMoreSpritesFast ; Hit sprite limit, bail out
    
    ; D is now address of OAM table destination
    asl
    asl
    adc #spriteTable
    tcd
    
    ; Check Y is in range
    lda objPosY, x
    
    ; Y scroll and offset
    adc a:SPRITEDEF_Y, y
    sec
    sbc spriteScrollY
    
    cmp #$ffe0  ; Check if in negative Y region of screen
    bcs _SpriteYInRangeFast
    cmp #$00e0  ; If greater than 224, it's offscreen
    bcs _NoMoreSpritesFast
    
_SpriteYInRangeFast:
    sta z:OAM_Y
    
    ; Check X is in range or is negative (65536 - 32 = 65504 = $ffe0)
    lda objPosX, x
    
    ; X scroll and offset
    clc
    adc a:SPRITEDEF_X, y
    sec
    sbc spriteScrollX
    
    cmp #$ffe0  ; Check if in negative X region of screen
    bcs _SpriteXInRangeFast
    cmp #$0100  ; If greater than 256, it's offscreen
    bcs _NoMoreSpritesFast
    
_SpriteXInRangeFast:
    
    sep #$20
    
    ; X position
    sta z:OAM_X
    
    ; High OAM scratch
    xba
    and #$01
    ora a:SPRITEDEF_SIZE, y
    
    phx
    
    ldx _oamPos
    sta a:_spriteTableScratch, x
    
    plx
    
    ; Attributes
    rep #$21
    lda a:SPRITEDEF_ATTRIBUTES, y
    sta z:OAM_ATTRIBUTES
    
    ; Increment position in OAM tables
    inc _oamPos
    
    rts

_NoMoreSpritesFast:
    
    sep #$40 ; Overflow flag indicates sprite was not placed
    rts

; ============================================================================
; BeginSprites - Must call before adding any sprites, and not when preserving
;                them
; ---------------------------------------------------------------------------
; Expects anything, clobbers nothing
; ============================================================================
BeginSprites:
    php
    rep #$30
    
    stz _oamPos
    
    plp
    rts


; ============================================================================
; FinalizeSprites - Must call after sprites for this frame are added
; ---------------------------------------------------------------------------
; Expects anything, clobbers A, X, Y, D
; ============================================================================
FinalizeSprites:
    php
    
    rep #$31
    
    ; D has to be zero
    lda #$0000
    tcd
    
    lda _oamPos
    bmi _DontFinalize ; Already finalized
    
    asl
    asl
    tax
    ldy _oamPos
    
    sep #$20
    
    .ifdef OFFSCREEN_SPRITES_BY_X
    ; Set X to be off-screen (after the high table is set properly)
    lda #$01
    .else
    ; Set Y to be off-screen (NTSC/224-line only)
    lda #$e0
    .endif
    
_MoveOffscreenLoop:
    .ifdef OFFSCREEN_SPRITES_BY_X
    sta spriteTable, x
    sta _spriteTableScratch, y
    .else
    sta spriteTable + OAM_Y, x
    .endif
    inx
    inx
    inx
    inx
    iny
    cpy _lastOamPos ; Only clear up to the last "dirty" slot
    bcc _MoveOffscreenLoop
    
    ; Set OAM high table as appropriate
    
_SkipOffscreenLoop:
    
    lda _oamPos
    sta _lastOamPos
    
    sep #$30
    
    ldx #$00 ; High table byte
    ldy #$00 ; Scratch table byte
    
_FinalizeHighTableLoop:
    
    lda _spriteTableScratch + 3, y
    asl
    asl
    ora _spriteTableScratch + 2, y
    asl
    asl
    ora _spriteTableScratch + 1, y
    asl
    asl
    ora _spriteTableScratch + 0, y
    sta spriteTableHigh, x
    
    inx
    iny
    iny
    iny
    iny
    bpl _FinalizeHighTableLoop
    
    rep #$30
    
    lda #$8000
    sta _oamPos

_DontFinalize:
    
    plp
    rts
