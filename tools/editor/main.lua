--[[
See LICENSE for important information on distribution.
--]]

local app = require "src.app"

local MainScreen = require "src.screens.mainscreen"
local SavePromptScreen = require "src.screens.savepromptscreen"

function love.load()
    app.loadResources()
    app.setHome(MainScreen)
    app.promptScreenClass = SavePromptScreen
    app.setScreen(MainScreen:new())
end
