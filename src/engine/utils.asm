; Utility functions and macros

.p816   ; 65816 processor
.a16
.i16
.smart

.export WorldToTileCoords, PrintHexByteToTilemap, PrintHexToTilemap, SineTable, CosineTable, Sine16Table, Cosine16Table, RandomByte, scratch

.segment "LORAM"

scratch: .res 6
rngIndex: .res 1

.segment "CODE0"


; ============================================================================
; WorldToTileCoords - Converts integer X/Y coordinates to a 16x16 tile index
;                     within a 128x128 region.
; ---------------------------------------------------------------------------
; In: X - X coordinate
;     Y - Y coordinate
; ---------------------------------------------------------------------------
; Expects anything, clobbers A
; ============================================================================
WorldToTileCoords:
    php
    rep #$30
    
    txa
    lsr
    lsr
    lsr
    lsr
    and #%11111
    sta scratch
    
    tya
    asl
    and #%0000001111100000
    ora scratch
    plp
    
    rts


; ============================================================================
; PrintHexByteToTilemap - Put 8-bit value into a tilemap
; ---------------------------------------------------------------------------
; In: X - Value to display
;     Y - Tilemap and offset to print to
; ---------------------------------------------------------------------------
; Expects XY16, clobbers A
; ============================================================================
PrintHexByteToTilemap:
    php
    sep #$20
    
    txa
    and #$0f
    inc a
    inc a
    sta a:2, y
    
    txa
    lsr
    lsr
    lsr
    lsr
    and #$0f
    inc a
    inc a
    sta a:0, y
    
    plp
    rts


; ============================================================================
; PrintHexToTilemap - Put 16-bit value into a tilemap
; ---------------------------------------------------------------------------
; In: X - Value to display
;     Y - Tilemap and offset to print to
; ---------------------------------------------------------------------------
; Expects XY16, clobbers A
; ============================================================================
PrintHexToTilemap:
    php
    sep #$20
    
    ; High byte
    
    txa
    and #$0f
    inc a
    inc a
    sta a:6, y
    
    txa
    lsr
    lsr
    lsr
    lsr
    and #$0f
    inc a
    inc a
    sta a:4, y
    
    ; Low byte
    
    rep #$20
    txa
    sep #$20
    xba
    and #$0f
    inc a
    inc a
    sta a:2, y
    
    rep #$20
    txa
    sep #$20
    xba
    lsr
    lsr
    lsr
    lsr
    and #$0f
    inc a
    inc a
    sta a:0, y
    
    plp
    rts


; ============================================================================
; RandomByte - Draws an (extremely pseudo) random number
; ---------------------------------------------------------------------------
; Expects anything but chops XY, clobbers A
; ============================================================================
RandomByte:
    php
    stx scratch
    
    sep #$30
    inc rngIndex
    ldx rngIndex
    rep #$30
    
    lda RandomTable, x
    
    ldx scratch
    and #$00ff
    
    plp
    rts


RandomTable:
.byte $11, $e3, $e1, $b7, $81, $3f, $03, $b9
.byte $dd, $91, $b8, $4c, $fe, $a0, $e2, $85
.byte $04, $55, $28, $25, $ad, $41, $4b, $d3
.byte $a5, $10, $6d, $54, $cc, $f3, $76, $98
.byte $6a, $94, $4e, $e5, $2a, $9f, $45, $15
.byte $5b, $5e, $bb, $c3, $14, $2b, $02, $06
.byte $bf, $7a, $93, $a3, $6e, $d2, $db, $d9
.byte $70, $44, $2d, $71, $97, $c1, $af, $e0
.byte $ea, $35, $b3, $51, $56, $a4, $78, $f1
.byte $4f, $0c, $de, $f4, $2e, $be, $49, $6b
.byte $60, $ee, $fb, $0e, $fd, $89, $d1, $a9
.byte $9d, $77, $c5, $3c, $7d, $d5, $e9, $00
.byte $7e, $01, $31, $19, $24, $50, $34, $d0
.byte $59, $95, $47, $ff, $0a, $13, $17, $d8
.byte $96, $0d, $a2, $72, $84, $69, $46, $ce
.byte $80, $64, $68, $6c, $ab, $66, $37, $cb
.byte $7c, $07, $9c, $c0, $3b, $21, $f6, $fc
.byte $48, $da, $79, $5c, $99, $8e, $20, $8d
.byte $e4, $74, $26, $c7, $8a, $32, $bd, $33
.byte $39, $7b, $90, $83, $3d, $b5, $58, $dc
.byte $a6, $67, $1c, $61, $29, $8c, $b4, $62
.byte $43, $87, $52, $b6, $ca, $9a, $30, $b1
.byte $40, $3e, $bc, $4d, $8b, $a1, $1f, $65
.byte $73, $4a, $86, $c6, $f2, $2c, $1b, $1e
.byte $f0, $df, $7f, $1d, $cd, $a8, $e6, $12
.byte $82, $16, $b2, $fa, $e8, $3a, $aa, $ac
.byte $d7, $c9, $08, $9e, $eb, $5a, $38, $ed
.byte $5d, $ae, $88, $09, $1a, $f7, $ba, $0b
.byte $36, $e7, $0f, $92, $a7, $23, $c4, $8f
.byte $75, $cf, $63, $9b, $c8, $18, $ef, $22
.byte $b0, $53, $27, $6f, $d6, $2f, $57, $d4
.byte $f8, $5f, $42, $05, $f5, $c2, $f9, $ec

SineTable:
.byte $80, $83, $86, $89, $8c, $8f, $92, $95
.byte $98, $9b, $9e, $a2, $a5, $a7, $aa, $ad
.byte $b0, $b3, $b6, $b9, $bc, $be, $c1, $c4
.byte $c6, $c9, $cb, $ce, $d0, $d3, $d5, $d7
.byte $da, $dc, $de, $e0, $e2, $e4, $e6, $e8
.byte $ea, $eb, $ed, $ee, $f0, $f1, $f3, $f4
.byte $f5, $f6, $f8, $f9, $fa, $fa, $fb, $fc
.byte $fd, $fd, $fe, $fe, $fe, $ff, $ff, $ff
CosineTable:
.byte $ff, $ff, $ff, $ff, $fe, $fe, $fe, $fd
.byte $fd, $fc, $fb, $fa, $fa, $f9, $f8, $f6
.byte $f5, $f4, $f3, $f1, $f0, $ee, $ed, $eb
.byte $ea, $e8, $e6, $e4, $e2, $e0, $de, $dc
.byte $da, $d7, $d5, $d3, $d0, $ce, $cb, $c9
.byte $c6, $c4, $c1, $be, $bc, $b9, $b6, $b3
.byte $b0, $ad, $aa, $a7, $a5, $a2, $9e, $9b
.byte $98, $95, $92, $8f, $8c, $89, $86, $83
.byte $80, $7c, $79, $76, $73, $70, $6d, $6a
.byte $67, $64, $61, $5d, $5a, $58, $55, $52
.byte $4f, $4c, $49, $46, $43, $41, $3e, $3b
.byte $39, $36, $34, $31, $2f, $2c, $2a, $28
.byte $25, $23, $21, $1f, $1d, $1b, $19, $17
.byte $15, $14, $12, $11, $0f, $0e, $0c, $0b
.byte $0a, $09, $07, $06, $05, $05, $04, $03
.byte $02, $02, $01, $01, $01, $00, $00, $00
.byte $00, $00, $00, $00, $01, $01, $01, $02
.byte $02, $03, $04, $05, $05, $06, $07, $09
.byte $0a, $0b, $0c, $0e, $0f, $11, $12, $14
.byte $15, $17, $19, $1b, $1d, $1f, $21, $23
.byte $25, $28, $2a, $2c, $2f, $31, $34, $36
.byte $39, $3b, $3e, $41, $43, $46, $49, $4c
.byte $4f, $52, $55, $58, $5a, $5d, $61, $64
.byte $67, $6a, $6d, $70, $73, $76, $79, $7c
.byte $80, $83, $86, $89, $8c, $8f, $92, $95
.byte $98, $9b, $9e, $a2, $a5, $a7, $aa, $ad
.byte $b0, $b3, $b6, $b9, $bc, $be, $c1, $c4
.byte $c6, $c9, $cb, $ce, $d0, $d3, $d5, $d7
.byte $da, $dc, $de, $e0, $e2, $e4, $e6, $e8
.byte $ea, $eb, $ed, $ee, $f0, $f1, $f3, $f4
.byte $f5, $f6, $f8, $f9, $fa, $fa, $fb, $fc
.byte $fd, $fd, $fe, $fe, $fe, $ff, $ff, $ff

Sine16Table:
.word $0000, $0003, $0006, $0009, $000c, $000f, $0012, $0015
.word $0018, $001b, $001e, $0022, $0025, $0027, $002a, $002d
.word $0030, $0033, $0036, $0039, $003c, $003e, $0041, $0044
.word $0046, $0049, $004b, $004e, $0050, $0053, $0055, $0057
.word $005a, $005c, $005e, $0060, $0062, $0064, $0066, $0068
.word $006a, $006b, $006d, $006e, $0070, $0071, $0073, $0074
.word $0075, $0076, $0078, $0079, $007a, $007a, $007b, $007c
.word $007d, $007d, $007e, $007e, $007e, $007f, $007f, $007f
Cosine16Table:
.word $007f, $007f, $007f, $007f, $007e, $007e, $007e, $007d
.word $007d, $007c, $007b, $007a, $007a, $0079, $0078, $0076
.word $0075, $0074, $0073, $0071, $0070, $006e, $006d, $006b
.word $006a, $0068, $0066, $0064, $0062, $0060, $005e, $005c
.word $005a, $0057, $0055, $0053, $0050, $004e, $004b, $0049
.word $0046, $0044, $0041, $003e, $003c, $0039, $0036, $0033
.word $0030, $002d, $002a, $0027, $0025, $0022, $001e, $001b
.word $0018, $0015, $0012, $000f, $000c, $0009, $0006, $0003
.word $0000, $fffc, $fff9, $fff6, $fff3, $fff0, $ffed, $ffea
.word $ffe7, $ffe4, $ffe1, $ffdd, $ffda, $ffd8, $ffd5, $ffd2
.word $ffcf, $ffcc, $ffc9, $ffc6, $ffc3, $ffc1, $ffbe, $ffbb
.word $ffb9, $ffb6, $ffb4, $ffb1, $ffaf, $ffac, $ffaa, $ffa8
.word $ffa5, $ffa3, $ffa1, $ff9f, $ff9d, $ff9b, $ff99, $ff97
.word $ff95, $ff94, $ff92, $ff91, $ff8f, $ff8e, $ff8c, $ff8b
.word $ff8a, $ff89, $ff87, $ff86, $ff85, $ff85, $ff84, $ff83
.word $ff82, $ff82, $ff81, $ff81, $ff81, $ff80, $ff80, $ff80
.word $ff80, $ff80, $ff80, $ff80, $ff81, $ff81, $ff81, $ff82
.word $ff82, $ff83, $ff84, $ff85, $ff85, $ff86, $ff87, $ff89
.word $ff8a, $ff8b, $ff8c, $ff8e, $ff8f, $ff91, $ff92, $ff94
.word $ff95, $ff97, $ff99, $ff9b, $ff9d, $ff9f, $ffa1, $ffa3
.word $ffa5, $ffa8, $ffaa, $ffac, $ffaf, $ffb1, $ffb4, $ffb6
.word $ffb9, $ffbb, $ffbe, $ffc1, $ffc3, $ffc6, $ffc9, $ffcc
.word $ffcf, $ffd2, $ffd5, $ffd8, $ffda, $ffdd, $ffe1, $ffe4
.word $ffe7, $ffea, $ffed, $fff0, $fff3, $fff6, $fff9, $fffc
.word $0000, $0003, $0006, $0009, $000c, $000f, $0012, $0015
.word $0018, $001b, $001e, $0022, $0025, $0027, $002a, $002d
.word $0030, $0033, $0036, $0039, $003c, $003e, $0041, $0044
.word $0046, $0049, $004b, $004e, $0050, $0053, $0055, $0057
.word $005a, $005c, $005e, $0060, $0062, $0064, $0066, $0068
.word $006a, $006b, $006d, $006e, $0070, $0071, $0073, $0074
.word $0075, $0076, $0078, $0079, $007a, $007a, $007b, $007c
.word $007d, $007d, $007e, $007e, $007e, $007f, $007f, $007f
