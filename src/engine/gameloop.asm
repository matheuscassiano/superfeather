
.p816   ; 65816 processor
.a16
.i16
.smart

.include "../../inc/engine/constants.inc"
.include "../../inc/engine/dma.inc"
.include "../../inc/engine/crashhandler.inc"

.import SpcBusyHeartbeat, DMAInitialize, DMAPrepare, DMADoTransfer, CreateObjectPool, ResetAllObjects, ClearSprites, BeginSprites, FinalizeSprites, SpcUpdateSounds
.import __LORAM_SIZE__, __HIRAM_SIZE__, bg1ScrollX, bg1ScrollY, bg2ScrollX, bg2ScrollY, bg3ScrollX, bg3ScrollY, dmaTableIndex, dmaTotalSize, spriteTable

.export Gameloop, VBlank, WaitVBlank
.export sceneTics, sceneTicsHigh, frameReady, slowdown, cyclesLeft, vblankEnd, screenBrightness, switchScene, sceneInitFunc, sceneThinkFunc, inputRaw, inputPrev, inputTap, inputUp, input1Raw, input1Prev, input1Tap, input1Up, input2Raw, input2Prev, input2Tap, input2Up

.segment "LORAM"

sceneTics: .res 2
sceneTicsHigh: .res 2
frameReady: .res 2
slowdown: .res 2
cyclesLeft: .res 2   ; "Idle" is 0x17de, or 6110. x10 to get approx real cycles
vblankEnd: .res 2
screenBrightness: .res 1
switchScene: .res 2
vblankLock: .res 1

.segment "LORAM_KEEP"
sceneInitFunc: .res 2
sceneThinkFunc: .res 2
inputRaw:
input1Raw: .res 2
inputPrev:
input1Prev: .res 2
inputTap:
input1Tap: .res 2
inputUp:
input1Up: .res 2
input2Raw: .res 2
input2Prev: .res 2
input2Tap: .res 2
input2Up: .res 2

.segment "DATA0"

    ;    123456789012345678901234567890  123456789012345678901234567890
ErrorOutOfVBlank:
    .byte "Whoa, ran out of VBlank time!   Not good for CPU rev1 users!  "

.segment "CODE0"


; ============================================================================
; Expects AXY16, clobbers basically everything
; ============================================================================
Gameloop:
    jsr _LoadNewScene
    
    rep #$30
    
    pea _SceneInitDone-1
    jmp (sceneInitFunc)
    
_SceneInitDone:
_Infinity:
    
    sep #$20
    rep #$10
    
    lda switchScene
    bne Gameloop
    
    jsr DMAPrepare
    
    rep #$30
    
    lda input1Prev
    eor input1Raw
    and input1Raw
    sta input1Tap
    
    lda input1Raw
    eor input1Prev
    and input1Prev
    sta input1Up
    
    lda input2Prev
    eor input2Raw
    and input2Raw
    sta input2Tap
    
    lda input2Raw
    eor input2Prev
    and input2Prev
    sta input2Up
    
    inc bg1ScrollY
    inc bg2ScrollY
    inc bg3ScrollY
    
    pea _SceneThinkDone-1
    jmp (sceneThinkFunc)
    
_SceneThinkDone:
    rep #$30
    
    dec bg1ScrollY
    dec bg2ScrollY
    dec bg3ScrollY

    jsr SpcUpdateSounds
    
    inc sceneTics
    bne _DontIncHighTic
    inc sceneTicsHigh
    
_DontIncHighTic:
    
    jsr WaitVBlank
    
    jmp _Infinity

_ZeroByte:
    .byte $00


; ============================================================================
; Expects anything, clobbers A, X, Y
; ============================================================================
_LoadNewScene:
    php
    
    rep #$10
    sep #$20
    
    ; Reset HDMA enable
    stz $420c
    
    ; Turn off screen
    lda #%10000000
    sta $2100
    
    stz switchScene
    
    ; Clear game logic RAM (LoRAM)
    stz $2181 ; WRAM location 00 00 00
    stz $2182
    stz $2183
    
    ;     da ifttt Flags
    lda #%00001000 ; Fixed transfer (read same byte always)
    sta $4300
    lda #$80 ; WRAM write as dest
    sta $4301
    ldx #.loword(_ZeroByte) ; Source
    stx $4302
    lda #<.bank(_ZeroByte)
    sta $4304
    ldx #__LORAM_SIZE__ ; Write only through the disposable LoRAM portion
    stx $4305
    
    lda #$01
    sta $420b
    
    ; Clear game logic RAM (HiRAM)
    stz $2181 ; WRAM location 7e 20 00
    lda #$20
    sta $2182
    lda #$01
    sta $2183
    ldx #__HIRAM_SIZE__ ; Write only through the disposable HiRAM portion
    stx $4305
    
    rep #$30
    
    ; Initialize object function pointers or we may (will) crash
    jsr ResetAllObjects
    jsr CreateObjectPool
    ; Also initialize WRAM OAM
    jsr ClearSprites
    ; Additional initialization
    jsr DMAInitialize
    
    ; Fix BG offset
    dec bg1ScrollY
    dec bg2ScrollY
    dec bg3ScrollY
    
    plp
    rts


; ============================================================================
; WaitVBlank - Called after the current frame is processed to wait for VBlank.
;              If the IRQ-friendly wait is enabled, the routine will perform
;              cycle counting. Otherwise, the CPU will sleep to save power.
; ---------------------------------------------------------------------------
; Expects any, Clobbers A
; ============================================================================
WaitVBlank:

.ifdef IRQ_UNSAFE_VBLANK_WAIT

    php
    
    sep #$20
    
    lda #$01
    sta frameReady
    
    wai
    
    stz slowdown
    
    plp
    rts

; ============================================================================
.else
; ============================================================================

    php
    
    sep #$20
    rep #$10
    
    lda #$01
    sta frameReady
    
    ldx #$0000
    
_CycleCountLoop:
    inx
    lda frameReady
    bne _CycleCountLoop
    
    stz slowdown
    
    bne _ZeroCycleCount
    stx cyclesLeft
    
    plp
    rts
    
_ZeroCycleCount:
    
    rep #$20
    stz cyclesLeft
    
    plp
    rts

.endif


; ============================================================================

_VBlankLockedJump:
    jmp _VBlankLocked

_FrameNotReadyJump:
    jmp _FrameNotReady


; ============================================================================
; Interrupt; can be called from any state
; ============================================================================
VBlank:
    jml _VBlankFast
_VBlankFast:
    phb
    phd
    rep #$30
    pha
    phx
    phy
    
    ; Set direct page
    lda #$2100
    tcd
    
    sep #$20
    
    lda $4210 ; Reset NMI flag
    
    ; Go to bank 80 so we can access registers and such
    lda #$80
    pha
    plb
    
    lda vblankLock ; To avoid infinite recursion
    bne _VBlankLockedJump
    inc vblankLock
    
    lda frameReady
    beq _FrameNotReadyJump
    
    ; Transferring sprites now means we waste less time waiting for autojoypad
    stz $02       ; set OAM address to 0
    stz $03

    ldy #$0400
    sty $4300       ; CPU -> PPU, auto increment, write 1 reg, $2104 (OAM Write)
    ldy #.loword(spriteTable)
    sty $4302
    lda #$01
    sta $4303       ; source offset
    ldy #$0220
    sty $4305       ; number of bytes to transfer
    lda #$7E
    sta $4304       ; bank address = $7E  (work RAM)
    lda #$01
    sta $420B       ;start DMA transfer
    
    ldx input1Raw
    stx input1Prev
    ldx input2Raw
    stx input2Prev
    
_WaitForInput:

    lda $4212
    bit #%00000001
    bne _WaitForInput ; If bit 0 is set, autojoypad is busy so we need to wait
    
    lda $4218
    sta input1Raw
    lda $4219
    sta input1Raw+1
    
    lda $421a
    sta input2Raw
    lda $421b
    sta input2Raw+1
    
    ; Scrolling
    lda bg1ScrollX
    sta $0d
    lda bg1ScrollX+1
    sta $0d
    lda bg1ScrollY
    sta $0e
    lda bg1ScrollY+1
    sta $0e
    
    lda bg2ScrollX
    sta $0f
    lda bg2ScrollX+1
    sta $0f
    lda bg2ScrollY
    sta $10
    lda bg2ScrollY+1
    sta $10
    
    lda bg3ScrollX
    sta $11
    lda bg3ScrollX+1
    sta $11
    lda bg3ScrollY
    sta $12
    lda bg3ScrollY+1
    sta $12
    
    jsr DMADoTransfer
    
    ; Set brightness based on desired value
    lda screenBrightness
    sta $00
    
    ; Get how far we went into VBlank
    lda $37
    lda $3d
    sta vblankEnd
    lda $3d
    and #$01
    sta vblankEnd+1
    
    ; Bit 7 set means VBlank is active, if this is no longer the case we spent too much time
    lda $4212
    and #%10000000
    beq _BlankTooLong
    
    bra _FrameDone
    
_FrameNotReady:
    
    inc slowdown
    jsr SpcBusyHeartbeat
    
_FrameDone:
    stz vblankLock
_VBlankLocked:
    stz frameReady
    
    rep #$30
    
    ply
    plx
    pla
    pld
    plb
    
    rti

_BlankTooLong:
    Macro_FatalError #.loword(ErrorOutOfVBlank), dmaTotalSize, dmaTableIndex, vblankEnd
