
.p816   ; 65816 processor
.a16
.i16
.smart

; Most source files should start with the above series of directives. This tells ca65 that we are
;  programming for the 65816 and that it should track use of rep/sep to determine the register sizes,
;  but that they should default to 16-bit.

.include "../../inc/engine/constants.inc"
.include "../../inc/engine/dma.inc"
.include "../../inc/engine/graphics.inc"

; For determining which object we want to spawn
.include "spawnids.inc"

.import SetupPpuRegisters:far, SetObjectPools, BeginSprites, UpdateObjects, FinalizeSprites, SpawnToken, screenBrightness, switchScene, inputRaw, inputTap

.export ObjectPoolSceneInit, ObjectPoolSceneThink

.segment "DATA2"

; Various data files, uploaded during scene initialization.

ObjectPoolsPal:
    .INCBIN "examples/objectpools/data/objectpools.pal"
ObjectPoolsPal_Len = * - ObjectPoolsPal

HudChar:
    .INCBIN "examples/objectpools/data/char.pic2"
HudChar_Len = * - HudChar

HudTilemap:
    .INCBIN "examples/objectpools/data/char.map"
HudTilemap_Len = * - HudTilemap

TokensChar:
    .INCBIN "examples/objectpools/data/tokens.pic"
TokensChar_Len = * - TokensChar

.segment "CODE0"

; These values are used to initialize the PPU registers, so we can set up the background mode and
;  address offsets we need, among other things. See documentation on the PPU registers for more
;  information.

;                            OBJSEL,                                 BGMODE,    BG1TILE,                                      BG2TILE,                                      BG3TILE,                                      BG4TILE,   BG12CHAR,                                      BG34CHAR,                                      MODE7SEL,  WIN12SEL,  WIN34SEL,  WINOBJSEL, WINBGLOG,  WINOBJLOG, DESTMAIN,  DESTSUB,   WINMAIN,   WINSUB,    CMATHSEL,  CMATHDEST, FIXEDCOLOR,        SETINI
Define_PpuDef SceneInitRegs, PPU_OBJSIZE_16_32 | PPU_OBJCHARADDR_0,  %00001001, PPU_TILEMAPSIZE_32_32 | PPU_TILEMAPADDR_6000, PPU_TILEMAPSIZE_32_32 | PPU_TILEMAPADDR_6800, PPU_TILEMAPSIZE_32_32 | PPU_TILEMAPADDR_7800, %00000000, PPU_BG13CHARADDR_3000 | PPU_BG24CHARADDR_3000, PPU_BG13CHARADDR_2000 | PPU_BG24CHARADDR_2000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00011111, %00000000, %00000000, %00000000, %00000000, %00000000, %0000000000000000, %00000000


; ============================================================================
; An important bit: Our object pool sizes! We're using four for this example.
; ============================================================================
_ObjectPoolDef:
    .word $0000, $0001, $0001, $0030, $0030, $0060, $0060, $0080, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff


; ============================================================================
; The initialization function, called once during startup of this scene
; ============================================================================
ObjectPoolSceneInit:
    php
    
    rep #$10
    sep #$20
    
    ; Initialize the registers using the data structure above.
    ldx #.loword(SceneInitRegs)
    jsl SetupPpuRegisters
    
    ; Set our object pool sizes (IMPORTANT)
    ldx #.loword(_ObjectPoolDef)
    jsr SetObjectPools

    ; Load palettes, VRAM data, etc. here.
    Macro_LoadPalette ObjectPoolsPal, #0, #ObjectPoolsPal_Len
    Macro_LoadVRAM HudChar, #$2000, #HudChar_Len
    Macro_LoadVRAM HudTilemap, #$7800, #HudTilemap_Len
    Macro_LoadVRAM TokensChar, #$0000, #TokensChar_Len
    
    plp
    rts


; ============================================================================
; The thinker function for this scene, called every frame.
; ============================================================================
ObjectPoolSceneThink:
    php
    
    ; Turn on the screen
    sep #$20
    lda #%00001111
    sta screenBrightness
    rep #$30
    
    ; User interaction: Check which button was pressed
    
    ; Spawn a Player Dragon object on A press
    lda inputRaw
    bit #INPUT_A
    beq _NoA
    ldy #SPAWNID_PLAYER_DRAGON
    jsr SpawnToken
    
_NoA:
    ; Spawn a Gem object on B press
    lda inputRaw
    bit #INPUT_B
    beq _NoB
    ldy #SPAWNID_GEM
    jsr SpawnToken
    
_NoB:
    ; Spawn a Spikeball object on X press
    lda inputRaw
    bit #INPUT_X
    beq _NoX
    ldy #SPAWNID_SPIKEBALL
    jsr SpawnToken
    
_NoX:
    ; Spawn a Sparkle object on Y press
    lda inputRaw
    bit #INPUT_Y
    beq _NoY
    ldy #SPAWNID_SPARKLE
    jsr SpawnToken
    
_NoY:
    lda inputTap
    bit #INPUT_SELECT
    beq _NoSelect
    ;; Restart the scene
    lda #$01
    sta switchScene

_NoSelect:

    ; Do our object processing as usual.
    jsr BeginSprites
    
    jsr UpdateObjects
    jsr FinalizeSprites
    
    plp
    rts
