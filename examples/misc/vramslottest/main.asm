
.p816   ; 65816 processor
.a16
.i16
.smart

.import Gameloop, sceneInitFunc, sceneThinkFunc, VramSlotTestSceneInit, VramSlotTestSceneThink

.export GameMain, SpcDriver

.segment "DATA3"

SpcDriver:
    .incbin "examples/misc/vramslottest/data/spc700.bin"

.segment "CODE0"


; ============================================================================
; GameMain is called once the engine has finished its own initialization. This space is for your game
;  to perform its own setup.
; ============================================================================
GameMain:
    
    lda #.loword(VramSlotTestSceneInit)
    sta sceneInitFunc
    lda #.loword(VramSlotTestSceneThink)
    sta sceneThinkFunc

_Forever:
    jsr Gameloop
    bra _Forever
